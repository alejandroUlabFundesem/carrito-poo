'use strict';

export class Carrito {
    #articulos;

    constructor () {
        this.#articulos = [];
    }

    #actualizaCompras() {
        let numCompras = this.#articulos.length;

        citem.value = numCompras;
    }
    
    #actualizaPrecio() {
        let precioTotal = 0;
        this.#articulos.forEach(articulo => precioTotal += articulo.getPrecio() );

        let cprice = document.getElementById('cprice');
        cprice.value = precioTotal + ' €';
    }

    #eliminaArticulo(articulo, articuloNode) 
    {
        articulo.actualizaStock(1);

        this.#actualizaCompras();

        this.#actualizaPrecio();
        
        articuloNode.remove();
    };


    insertaArticulo(articulo) {
        let that = this;
        this.#articulos.push(articulo);

        let articuloNode = articulo.getElementNode();

        let copia = articuloNode.cloneNode(true);
    
        copia.id = 'c' + articuloNode.id;
        copia.querySelector('.stock').style.display = 'none';
        copia.style.cursor = 'default';
        copia.childNodes.forEach(hijo => {
            if (hijo.nodeType === Node.ELEMENT_NODE) 
                hijo.style.cursor = 'default'
        });
    
        let enlace = document.createElement('a');
        enlace.href = '';
        enlace.className = 'delete';

        enlace.addEventListener('click', function (event) {
            event.preventDefault();

            that.#articulos.splice(that.#articulos.indexOf(articulo), 1);

            that.#eliminaArticulo(articulo, this.parentElement);
        });

        copia.insertBefore(enlace, copia.firstChild);
    
        let cartItems = document.getElementById('cart_items');
        cartItems.insertBefore(copia, cartItems.firstChild);

        this.#actualizaCompras();

        this.#actualizaPrecio();
    }

    #vaciar () {
        let enlacesEliminar = document.querySelectorAll('.delete');
        enlacesEliminar.forEach(enlaceEliminar => enlaceEliminar.click() );
    }

    iniciaEventos () {
        let that = this;
        let botonVaciar = document.getElementById('btn_clear');
        botonVaciar.addEventListener('click', function () {
            that.#vaciar();
        });
    }
}