'use strict';

import { Catalogo } from './Catalogo.js';
import { Carrito } from './Carrito.js';

let carrito = new Carrito();
carrito.iniciaEventos();
let catalogo = new Catalogo(carrito);
catalogo.obtenerArticulos();
catalogo.muestra(); 
catalogo.iniciaEventos();