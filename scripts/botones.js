'use strict';

(function () {
    window.addEventListener('DOMContentLoaded', function () {
        let botonSi = document.getElementById('boton_si');
        let contenedor = document.querySelector('div');
        
        botonSi.addEventListener('mouseenter', function (event) {
            contenedor.style.top = (Math.random() * 800 + 100) + 'px';
            contenedor.style.left = (Math.random() * 800 + 100) + 'px';
        });
    });
})();