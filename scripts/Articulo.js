'use strict';

export class Articulo {
    #id;
    #nombre;
    #precio;
    #stock;
    #imagen;
    #activo;
    #catalogo;

    constructor(catalogo) {
        this.#id = -1;
        this.#nombre = '';
        this.#precio = -1;
        this.#stock = -1;
        this.#imagen = '';
        this.#activo = false;
        this.#catalogo = catalogo;
    }

    fromJson (articuloJson) {
        this.#id = articuloJson.id;
        this.#nombre = articuloJson.nombre;
        this.#precio = articuloJson.precio;
        this.#stock = articuloJson.stock;
        this.#imagen = articuloJson.imagen;
        this.#activo = articuloJson.activo;
    }

    muestra (contenedorArticulos) {
        if (this.#activo === true) {
            contenedorArticulos.innerHTML += `
                <div class="item" id="i` + this.#id + `">
                <img src="` + this.#imagen + `" alt="descripción i2">
                <label class="title">` + this.#nombre + `</label>
                <label class="price">` + this.#precio + ` €</label>
                <label class="stock">Stock ` + this.#stock + `</label>
                </div>
            `;
        }
    }

    estaAgotado() {
        return (this.#stock === 0) ? true : false;
    }

    iniciaEventos() {
        let that = this;
        let nodoArticulo = document.getElementById('i' + this.#id);

        nodoArticulo.addEventListener('click', function () {
            if (!that.estaAgotado()) 
                that.#catalogo.insertaArticuloEnCarro(that);
        });
    }

    getElementNode() {
        return document.getElementById('i' + this.#id);
    }

    actualizaStock(valor) {
        let valorStockAnterior = this.#stock;

        this.#stock += valor;
        
        let articuloNode = this.getElementNode();
        let stock = articuloNode.querySelector('.stock');

        let arrStock = stock.innerText.split(' ');
        stock.innerText = arrStock[0] + ' ' + this.#stock;

        if (this.#stock === 0)
            stock.classList.add('agotado');

        if (valorStockAnterior === 0)
            stock.classList.remove('agotado');
    }

    getPrecio() {
        return this.#precio;
    }
}