'use strict';

import { GeneradorArticulos } from "./GeneradorArticulos.js";
import { Articulo } from "./Articulo.js";

export class Catalogo
{
    #articulos;
    #carrito;

    constructor(carrito) {
        this.#articulos = [];
        this.#carrito = carrito;
    }

    obtenerArticulos() {
        let articulosJSON = JSON.parse(GeneradorArticulos.getArticulos());
        articulosJSON.data.forEach(articuloJson => {
            let articulo = new Articulo(this);
            articulo.fromJson(articuloJson);
            this.#articulos.push(articulo);
        });
    }

    muestra() {
        let contenedorArticulos = document.getElementById('item_container');

        this.#articulos.forEach(articulo => articulo.muestra(contenedorArticulos) );

        contenedorArticulos.innerHTML += '<div class="clear"></div>';
    }

    iniciaEventos() {
        this.#articulos.forEach(articulo => articulo.iniciaEventos() );
    }

    insertaArticuloEnCarro(articulo) {
        this.#carrito.insertaArticulo(articulo);

        articulo.actualizaStock(-1);
/*    
        actualizaStock(articulo, -1);
    
        actualizaCompras(1);
    
        actualizaPrecioTotal(articulo, true);
        */
    }
}